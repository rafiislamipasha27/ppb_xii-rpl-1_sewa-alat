import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_arin/screens/product_screen.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFEEEEEE),
        title: Row(
          children: [
            Text(
              'Logo',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold, // Atur warna teks logo menjadi hitam
              ),
            ),
            Spacer(),
            SizedBox(width: 10),
            Image.asset('assets/cart.png', width: 30, height: 30),
            SizedBox(width: 10),
            Image.asset('assets/profile.png', width: 30, height: 30),
          ],
        ),
        elevation: 0,
      ),
      backgroundColor: Color(0xFFEEEEEE),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(7.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                border: Border.all(color: Colors.white),
                color: Colors.white,
              ),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.search),
                  ),
                  Expanded(
                    child: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Cari apapun yang Anda suka',
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: GridView.count(
              crossAxisCount: 2,
              children: List.generate(6, (index) {
                final productImages = [
                  'assets/product_images/product1.png',
                  'assets/product_images/product2.png',
                  'assets/product_images/product3.png',
                  'assets/product_images/product4.png',
                  'assets/product_images/product5.png',
                  'assets/product_images/product6.png',
                  // Tambahkan gambar lainnya ke sini sesuai kebutuhan
                ];
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ProductScreen(),
                      ),
                    );
                  },
                  child: Card(
                    margin: EdgeInsets.all(10),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15), // Tambahkan border radius di sini
                      side: BorderSide(color: Colors.white),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start, // Mengatur teks menjadi rata kiri secara vertikal
                          children: [
                            Center( // Mengatur gambar berada di tengah
                              child: Padding(
                                padding: EdgeInsets.only(bottom: 13),
                                child: Image.asset(productImages[index % productImages.length]),
                              )
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 6),
                              child: Text(
                                'Keterangan Produk',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold, // Membuat teks keterangan produk menjadi bold
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 20),
                              child: Text(
                                'Harga: \$100',
                              ),
                            ),
                            Text(
                              'Alamat: Alamat Produk',
                            ),
                          ],
                        ),
                      )
                    )
                  ),
                );
              }),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/home.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/comment.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/money.png')),
            label: '',
          ),
        ],
        currentIndex: 0,
        selectedItemColor: Colors.blue,
        onTap: (int index) {
          // Handle navigation here
        },
      ),
    );
  }
}

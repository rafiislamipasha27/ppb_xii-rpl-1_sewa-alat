import 'package:flutter/material.dart';

class ProductScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFEEEEEE),
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Image.asset(
            'assets/back.png',
            width: 24,
            height: 24,
            color: Colors.black, // Mengatur warna ikon "Back" menjadi hitam
          ),
        ),
      ),
      backgroundColor: Color(0xFFEEEEEE),
      body: Center(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(30),
          child: Card(
            margin: EdgeInsets.all(20), // Tambahkan margin pada card
            color: Colors.white, // Atur latar belakang Card menjadi putih
            child: Column(
              children: [
                Container(
                  width: 300,
                  height: 300,
                  child: Center(
                    child: Image.asset(
                      'assets/product_images/product_2.webp',
                      width: 300, // Ubah sesuai kebutuhan
                      height: 300, // Ubah sesuai kebutuhan
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.centerLeft, // Mengatur teks di sisi kiri
                  child: Padding (
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      'Keterangan Produk',
                      style: TextStyle(
                        fontSize: 25, // Ukuran teks medium
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                ),
                Padding(padding: EdgeInsets.only(bottom: 30)),
                Align(
                  alignment: Alignment.centerLeft, // Mengatur teks di sisi kiri
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      'Harga: \$100',
                      style: TextStyle(fontSize: 18),
                    ),
                  )
                ),
                Align(
                  alignment: Alignment.centerLeft, // Mengatur teks di sisi kiri
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      'Alamat: Alamat Produk',
                      style: TextStyle(fontSize: 16),
                    ),
                  )
                ),
              ],
            ),
          ),
        )
      ),
      bottomNavigationBar: Container(
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Padding(padding: EdgeInsets.only(left: 60)),
                Image.asset(
                  'assets/love.png',
                  width: 24,
                  height: 24,
                ),
                Padding(padding: EdgeInsets.only(left: 30)),
                Image.asset(
                  'assets/cart.png',
                  width: 24,
                  height: 24,
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.only(left: 83, right: 83, top: 16, bottom: 16),
              color: Colors.black,
              child: Text(
                'Beli Sekarang',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
